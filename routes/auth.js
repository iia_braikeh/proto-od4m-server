const express = require("express");
const router = express.Router();
const passport = require("../passport/setup");


const CLIENT_URL = process.env.NODE_ENV === "production" ? process.env.CLIENT_PROD_URL : process.env.CLIENT_URL;

router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

// GET /auth/openstreetmap
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in OpenStreetMap authentication will involve redirecting
//   the user to openstreetmap.org.  After authorization, OpenStreetMap will redirect the user
//   back to this application at /auth/openstreetmap/callback
router.get(
    "/openstreetmap",
    passport.authenticate("openstreetmap")
    // ,
    // function (req, res) {
    //     // The request will be redirected to OpenStreetMap for authentication, so this
    //     // function will not be called.
    // }
);

// GET /auth/openstreetmap/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
router.get(
    "/openstreetmap/callback",
    passport.authenticate("openstreetmap", {
        successRedirect: `${CLIENT_URL}`,
        // successRedirect: `${process.env.SERVER_URL}/api/permissions`,
        failureRedirect: "/api/login"
    })
);
//     function (req, res) {
//         //     // res.redirect(`${process.env.CLIENT_URL}/profile`);
//         //     // res.redirect('/profile');
//         //     console.log(req.headers['host'])
//         //     console.log(req.url)
//         //     res.redirect('/profile');
//         //     // res.redirect(req.headers['host'] + req.url);
//         //     // res.redirect(`${process.env.REACT_URL}`);
//     }
// );

// app.get("/auth/redirect", ensureAuthenticated, (req, res) => {
//     console.log("getting user data!");
//     console.log(req.user);
//     res.redirect(`${process.env.CLIENT_URL}/profile`);
// });

router.get("/logout", function (req, res) {
    req.logout();
    req.session.destroy();
    res.redirect(`${CLIENT_URL}`);
});

/// redirection temporaire
router.get("/login", function (req, res) {
    res.redirect("/auth/openstreetmap");
});

module.exports = router;