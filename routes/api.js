const express = require("express");
const router = express.Router();
const OsmRequest = require("osm-request");
const got = require("got");
const crypto = require("crypto");
const OAuth = require("oauth-1.0a");
const utils = require('../helpers/xml');
const osmUtils = require('../helpers/osm');
const { response } = require("express");
const apicache = require('apicache');
const hash = require('object-hash');
const { doBbox, getMoreNodes, pointInPolygon } = require("../helpers/geo");

let cache = apicache.options({
    headers: { 'cache-control': 'no-cache', },
}).middleware;

const OSM_API_END_POINT = process.env.OSM_API === 'dev' ? process.env.OSM_DEV_API_END_POINT : process.env.OSM_API_END_POINT;
const OSM_CONSUMER_KEY = process.env.OSM_API === 'dev' ? process.env.OSM_DEV_CONSUMER_KEY : process.env.OSM_CONSUMER_KEY;
const OSM_CONSUMER_SECRET = process.env.OSM_API === 'dev' ? process.env.OSM_DEV_CONSUMER_SECRET : process.env.OSM_CONSUMER_SECRET;

const oauth = OAuth({
    consumer: {
        key: OSM_CONSUMER_KEY,
        secret: OSM_CONSUMER_SECRET
    },
    signature_method: "HMAC-SHA1",
    hash_function: (baseString, key) =>
        crypto
            .createHmac("sha1", key)
            .update(baseString)
            .digest("base64")
});

router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

router.get("/credentials/check", ensureAuthenticated, function (req, res) {
    req.isAuthenticated() ? res.sendStatus(201) : res.sendStatus(401);
});

router.get("/user", ensureAuthenticated, function (req, res) {
    const { name, openstreetmapId, changesetsCount, ...restOfIt } = req.user;
    const user = { name, openstreetmapId, changesetsCount };
    res.json(user);
    console.log(OSM_API_END_POINT)
});

router.get("/permissions", ensureAuthenticated, function (req, res) {
    const token = {
        key: req.user.token,
        secret: req.user.tokenSecret
    };
    const url = `${OSM_API_END_POINT}/api/0.6/permissions`;
    (async () => {
        try {
            const response = await got(url, {
                headers: oauth.toHeader(oauth.authorize({ url, method: "GET" }, token)),
                responseType: "text"
            });
            console.log(response.body);
            res.send(response.body);
        } catch (error) {
            console.log(error); //=> 'Internal server error ...'
        }
    })();
});

/**
 * body request parameters
 * @param {String} wayId - way id ("way/xxx")
 * @param {Number} index - index of segment's first node
 * @param {String} elementId - element id ("way/xxx" or "relation/xxx")
 * @param {Object} newDoorNode - new door parameters
 * @param {Number} newDoorNode.lat - new door latitude
 * @param {Number} newDoorNode.lon - new door longitude
 * @param {Object} newDoorNode.tags - new door tags
 * @param {Object} newChangeset - new changeset parameters
 * @param {String} newChangeset.comment - new changeset comment
 * @param {String} newChangeset.createdBy - new changeset createdBy tag
 * @param {Object} newChangeset.tags - new changeset other tags
 */
router.post("/changeset", ensureAuthenticated, function (req, res) {
    console.log(" ==== put changeset ");
    console.log(req.body);
    const osm = new OsmRequest({
        endpoint: OSM_API_END_POINT,
        oauthConsumerKey: OSM_CONSUMER_KEY,
        oauthSecret: OSM_CONSUMER_SECRET,
        oauthUserToken: req.user.token,
        oauthUserTokenSecret: req.user.tokenSecret
    });

    // fix with typescript instance ?
    if (!req.body.newChangeset || !req.body.newDoorNode
        || req.body.wayId === undefined
        || req.body.index === undefined
        || req.body.elementId === undefined) {
        return res.status(400).json({
            status: 'error',
            error: 'req body cannot be empty',
        });
    }
    console.log("rem from cache", req.body.wayId, req.body.elementId)
    apicache.clear(req.body.wayId);
    apicache.clear(req.body.elementId);
    (async () => {
        try {
            // open changeset
            const changesetId = await osm.createChangeset(
                req.body.newChangeset.createdBy,
                req.body.newChangeset.comment,
                req.body.newChangeset.tags
            );

            const myway = await osm.fetchElement(req.body.wayId);
            let nodesOfWay = osm.getNodeIdsForWay(myway);
            console.log(nodesOfWay);

            // new door node
            const newnode = osm.createNodeElement(
                req.body.newDoorNode.lat,
                req.body.newDoorNode.lon,
                req.body.newDoorNode.tags);
            // send new door node
            const newNodeId = await osm.sendElement(newnode, changesetId);
            console.log("newNodeId : ", newNodeId);

            // modifyWayNodesList ;
            // on which segment of way ?
            const index = req.body.index;
            nodesOfWay.splice(index + 1, 0, `node/${newNodeId}`);
            const newway = osm.setNodeIdsForWay(myway, nodesOfWay);
            // send updated new door's way
            const newWayVersion = await osm.sendElement(newway, changesetId);

            // close changeset
            await osm.closeChangeset(changesetId);
            console.log('changeset closed');
            res.status(200).send({ newNodeId, newWayVersion });

        } catch (error) {
            console.log(error); //=> 'Internal server error ...'
        }
    })();
});

/**
 * body request parameters
 * @param {String} nodeId - node id ("node/xxx")
 * @param {Object} alterDoorNode - door parameters
 * @param {Number} alterDoorNode.lat - door latitude
 * @param {Number} alterDoorNode.lon - door longitude
 * @param {Object} alterDoorNode.tags - door tags
 * @param {Object} alterDoorNode.removedtags - door removed tags
 * @param {Object} alterDoorNode.version - node version
 * @param {Object} newChangeset - changeset parameters
 * @param {String} newChangeset.comment - changeset comment
 * @param {String} newChangeset.createdBy - changeset createdBy tag
 * @param {Object} newChangeset.tags - changeset other tags
 */
router.post("/alterdoorchangeset", ensureAuthenticated, function (req, res) {
    console.log(" ==== put alter door changeset ");
    console.log(req.body);
    const osm = new OsmRequest({
        endpoint: OSM_API_END_POINT,
        oauthConsumerKey: OSM_CONSUMER_KEY,
        oauthSecret: OSM_CONSUMER_SECRET,
        oauthUserToken: req.user.token,
        oauthUserTokenSecret: req.user.tokenSecret
    });

    // fix with typescript instance ?
    if (!req.body.newChangeset || !req.body.doorNode
        // || req.body.wayId === undefined
        // || req.body.index === undefined
        || req.body.elementId === undefined) {
        return res.status(400).json({
            status: 'error',
            error: 'req body cannot be empty',
        });
    }
    // apicache.clear(req.body.wayId);
    apicache.clear(req.body.elementId);
    (async () => {
        try {
            // open changeset
            const changesetId = await osm.createChangeset(
                req.body.newChangeset.createdBy,
                req.body.newChangeset.comment,
                req.body.newChangeset.tags
            );

            let mynode = await osm.fetchElement(req.body.nodeId);
            const hasBeenModified = mynode['$'].version > req.body.doorNode.version;
            if (hasBeenModified) {
                return res.status(409).send({
                    errorcode: 'different version',
                    error: 'Conflict : node has been modified meanwhile'
                });
            }

            if (req.body.doorNode.tags) {
                mynode = osm.setTags(mynode, req.body.doorNode.tags);
            }
            if (req.body.doorNode.removedtags && req.body.doorNode.removedtags.length !== 0) {
                req.body.doorNode.removedtags.forEach(
                    rm => mynode = osm.removeTag(mynode, rm)
                );
            }
            if (req.body.doorNode.lat && req.body.doorNode.lon) {
                mynode = osm.setCoordinates(mynode, req.body.newDoorNode.lat, req.body.newDoorNode.lon);
            }
            mynode = osm.setTimestampToNow(mynode);

            // send new version door node
            const newNodeId = await osm.sendElement(mynode, changesetId);
            // close changeset
            await osm.closeChangeset(changesetId);
            console.log('changeset closed');
            return res.status(200).send({ newNodeId });

        } catch (error) {
            console.log(error); //=> 'Internal server error ...'
            return res.status(500).send(error);
        }
    })();
});


router.get("/get/way/:id", cache('5 minutes'), ensureAuthenticated, function (req, res) {
    req.apicacheGroup = `way/${req.params.id}`;
    console.log(" ==== getting element ");
    const osm = new OsmRequest({
        endpoint: OSM_API_END_POINT,
        // oauthConsumerKey: OSM_CONSUMER_KEY,
        // oauthSecret: OSM_CONSUMER_SECRET,
        // oauthUserToken: "req.user.token",
        // oauthUserTokenSecret: "req.user.tokenSecret"
    });
    const possibleDoorTagKeys = ["addr:housenumber", "addr:street", "door", "entrance"];
    (async () => {
        try {
            let elementName = `way/${req.params.id}`;
            if (process.env.OSM_API === 'dev') { elementName = `way/4305627823`; }
            console.log("looking for element :  ", elementName);
            let element = await osm.fetchElement(elementName, { full: true });
            // console.log(element.way[0].nd)
            const tags = osm.getTags(element.way[0]);
            // let nodesElements = element.node.map(n => utils.flattenAttributes(n));
            const nodesElements = element.node.map(n => {
                let node = utils.flattenAttributes(n);
                const nodetags = n.tag ? n.tag.map(t => utils.flattenAttributes(t)) : null;
                node.tag = nodetags;
                // try
                // console.log('compare tags', nodetags, osm.getTags(n));
                // node.tag=osm.getTags(n);
                //
                return node;
            });
            const hasDoors = nodesElements.some(node =>
                node.tag && node.tag.some(t => possibleDoorTagKeys.includes(t.k)));
            // console.log(nodesElements)
            const way = element.way[0];
            const wayPositions = osmUtils.getPositionsOfWay(osm, way, nodesElements);
            const positions = [wayPositions];

            // find other nodes on or inside way, that are not part of way
            const bbox = doBbox(positions);
            let bboxElements = await osm.fetchMapByBbox(bbox[0], bbox[1], bbox[2], bbox[3]);
            const moreNodes = getMoreNodes(bboxElements,{outerPositions:positions},nodesElements);
            const hashed = hash.sha1(wayPositions);
            const refByPositionsHash = { [`${hashed}`]: `way/${way._id}` };

            let response = {
                way: element,
                id: elementName,
                tags: tags,
                positions: positions,
                refByPositionsHash: refByPositionsHash,
                nodes: nodesElements,
                moreNodes: moreNodes,
                hasDoors: hasDoors,
                date: Date.now(),
            }
            // nodesOfWay.map(n => {
            //     const that = utils.flattenAttributes(n);
            //     const index = nodesList.indexOf(`node/${that.id}`)
            //     response.positions[index] = [parseFloat(that.lat), parseFloat(that.lon)];
            //     response.nodes[index] = that;
            // });
            res.send(response)
        } catch (error) {
            console.log(error); //=> 'Internal server error ...'
        }
    })()
});

router.get("/get/relation/:id", cache('5 minutes'), ensureAuthenticated, function (req, res) {
    req.apicacheGroup = `relation/${req.params.id}`;
    console.log(" ==== getting element ");
    const osm = new OsmRequest({
        endpoint: OSM_API_END_POINT,
        // oauthConsumerKey: OSM_CONSUMER_KEY,
        // oauthSecret: OSM_CONSUMER_SECRET,
        // oauthUserToken: "req.user.token",
        // oauthUserTokenSecret: "req.user.tokenSecret"
    });
    (async () => {
        try {
            let elementName = `relation/${req.params.id}`;
            if (process.env.OSM_API === 'dev') { elementName = `relation/4304892610`; }
            console.log("looking for element :  ", elementName);
            let element = await osm.fetchElement(elementName, { full: true });
            // let members = element.relation[0].member;
            const tags = osm.getTags(element.relation[0]);
            const nodesElements = element.node.map(n => utils.flattenAttributes(n));
            // members = members.map(m => utils.flattenAttributes(m));
            // const outer = members.filter(m => m.role == 'outer');
            // const inner = members.filter(m => m.role == 'inner');
            // const outerWay = element.way.find(w => w._id == outer[0].ref)
            // const outPos = osmUtils.getPositionsOfWay(osm, outerWay, element.node);
            // let inPos = [];
            // inner.map(i => {
            //     const innerWay = element.way.find(w => w._id == i.ref);
            //     inPos.push(osmUtils.getPositionsOfWay(osm, innerWay, element.node));
            // }
            // );
            let positions = [];
            let refByPositionsHash = {};
            // members.forEach(i => {
            //     const nodesOfWayList = element.way.find(w => w._id == i.ref);
            //     positions.push(osmUtils.getPositionsOfWay(osm, nodesOfWayList, nodesElements));
            // });
            const ways = element.way;
            ways.forEach(way => {
                const wayPositions = osmUtils.getPositionsOfWay(osm, way, nodesElements);
                const hashed = hash.sha1(wayPositions);
                refByPositionsHash = {
                    ...refByPositionsHash,
                    [`${hashed}`]: `way/${way._id}`,
                };
                positions.push(wayPositions);
            });

            // find other nodes on or inside way, that are not part of way
            const bbox = doBbox(positions);
            let bboxElements = await osm.fetchMapByBbox(bbox[0], bbox[1], bbox[2], bbox[3]);

            // const outer = positions.filter(m => m.role == 'outer');
            // const inner = positions.filter(m => m.role == 'inner');
            // console.log("outer",outer)
            // console.log("inner",inner)
            // FIX: outer / inner tag from way
            const moreNodes = getMoreNodes(bboxElements,{outerPositions:positions},nodesElements);
            
            let response = {
                relation: element.relation[0],
                id: elementName,
                tags: tags,
                // positions: [[...outPos], ...inPos],
                positions: positions,
                refByPositionsHash: refByPositionsHash,
                nodes: nodesElements,
                moreNodes:moreNodes,
                hasDoors:hasDoord,
                date: Date.now(),
            }
            // console.log(response)
            res.send(response)
        } catch (error) {
            console.log(error); //=> 'Internal server error ...'
        }
    })()
});

router.get("/get/addr/:a/:b/:c/:d", ensureAuthenticated, function (req, res) {
    // req.apicacheGroup = `way/${req.params.id}`;
    console.log(" ==== getting element ");
    const osm = new OsmRequest({
        endpoint: OSM_API_END_POINT,
        // oauthConsumerKey: OSM_CONSUMER_KEY,
        // oauthSecret: OSM_CONSUMER_SECRET,
        // oauthUserToken: "req.user.token",
        // oauthUserTokenSecret: "req.user.tokenSecret"
    });
    (async () => {
        try {
            // left, bottom ,right ,top
            // -5.4398679733,43.483636914,-5.4344928265,43.4870542267
            let element = await osm.fetchMapByBbox(req.params.a, req.params.b, req.params.c, req.params.d);
            const streets = [];
            const cities = [];
            if (element.node) {
                element.node.forEach(
                    n => {
                        const tags = osm.getTags(n);
                        if (tags['addr:city']) {
                            cities.push(tags['addr:city']);
                        }
                        if (tags.admin_level) {
                            console.log(tags)
                            if (tags.admin_level >= 5
                                && tags.admin_level <= 10
                                && tags.name) { cities.push(tags.name); }
                        }
                    }
                );
            }
            if (element.way) {
                element.way.forEach(
                    w => {
                        const tags = osm.getTags(w);
                        if (tags['addr:city']) {
                            cities.push(tags['addr:city']);
                        }
                        if (tags.highway && tags.name) { streets.push(tags.name); }
                    }
                );
            }
            if (element.relation) {
                element.relation.forEach(
                    r => {
                        const tags = osm.getTags(r);
                        if (tags.highway && tags.name) { streets.push(tags.name); }
                    }
                );
            }
            let uniqueStreets = streets.filter((c, index) => {
                return streets.indexOf(c) === index;
            });
            let uniqueCities = cities.filter((c, index) => {
                return cities.indexOf(c) === index;
            });

            let response = {
                streets: uniqueStreets,
                cities: uniqueCities,
            }

            res.send(response)
        } catch (error) {
            console.log(error); //=> 'Internal server error ...'
        }
    })()
});

router.get("/get/highway/:a/:b/:c/:d", cache('30 minutes'), ensureAuthenticated, function (req, res) {
    req.apicacheGroup = `way/${req.params.id}`;
    console.log(" ==== getting element ");
    const osm = new OsmRequest({
        endpoint: OSM_API_END_POINT,
        // oauthConsumerKey: OSM_CONSUMER_KEY,
        // oauthSecret: OSM_CONSUMER_SECRET,
        // oauthUserToken: "req.user.token",
        // oauthUserTokenSecret: "req.user.tokenSecret"
    });
    (async () => {
        try {

            let element = await osm.fetchMapByBbox(req.params.a, req.params.b, req.params.c, req.params.d);
            let highway;
            if (element.way) {
                highway = element.way.filter(w => {
                    const tags = osm.getTags(w);
                    return tags.highway;
                });
            }
            let response = {
                highway: highway,
                // positions: positions,
                // refByPositionsHash: refByPositionsHash,
                date: Date.now(),
            }
            res.send(response)
        } catch (error) {
            console.log(error); //=> 'Internal server error ...'
        }
    })()
});

router.get("/userDetails", ensureAuthenticated, function (req, res) {

    console.log(" ==== getting user ");
    const osm = new OsmRequest({
        // endpoint: OSM_API_END_POINT,
        // oauthConsumerKey: OSM_CONSUMER_KEY,
        // oauthSecret: OSM_CONSUMER_SECRET,
        // oauthUserToken: "req.user.token",
        // oauthUserTokenSecret: "req.user.tokenSecret"
    });
    console.log(OSM_API_END_POINT);
    (async () => {
        try {
            let user = await osm.fetchUser(req.user.openstreetmapId);
            console.log(user)
            res.send(user)
        } catch (error) {
            console.log(error); //=> 'Internal server error ...'
        }
    })()
});

// Simple route middleware to ensure user is authenticated.
//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    // res.redirect('/login');
    // res.send({ "name": "jojo" });
    res.sendStatus(401);
}

module.exports = router;