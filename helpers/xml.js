/**
 * XML converted with the xmlToJson function contain some weird objects
 * Eg:
 * {
 *   $: {
 *     attribute1: 'value1',
 *     attribute2: 'value2'
 *   },
 *   attribute3: ['value3'],
 *   attribute4: ['value4'],
 * }
 *
 * That function flatten them
 * Eg:
 * {
 *   attribute1: 'value1',
 *   attribute2: 'value2',
 *   attribute3: 'value3',
 *   attribute4: 'value4',
 * }
 *
 * @param {Object} object
 * @return {Object}
 */
function flattenAttributes(object) {
    const flatObject = {
        ...object.$
    };

    Object.keys(object).forEach(key => {
        if (key === '$') return;
        if (!object[key]) return;
        if (object[key].length === 0) return;

        flatObject[key] = object[key][0];
    });

    return flatObject;
}

module.exports.flattenAttributes = flattenAttributes;