const utils = require('../helpers/xml');

function getPositionsOfWay(osm, way, nodesElements) {
    // nodes = nodes.map(n => utils.flattenAttributes(n));
    const nodesList = osm.getNodeIdsForWay(way);
    positions = [];

    nodesList.forEach(nodeId => {
        const node = nodesElements.find(n => `node/${n.id}` === nodeId)
        positions.push([parseFloat(node.lat), parseFloat(node.lon)]);
    })
    // nodesElements.map(n => {
    //     const that = (n);
    //     const index = nodesList.indexOf(`node/${that.id}`)
    //     if (index >= 0) {
    //         positions[index] = [parseFloat(that.lat), parseFloat(that.lon)];
    //     }
    // });

    return positions;
}

module.exports.getPositionsOfWay = getPositionsOfWay;


