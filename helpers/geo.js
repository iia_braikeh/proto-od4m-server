const utils = require('./xml');

function pointInPolygon(point, vs) {
    // ray-casting algorithm based on
    // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
    var x = point[0], y = point[1];
    var inside = false;
    for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
        var xi = vs[i][0], yi = vs[i][1];
        var xj = vs[j][0], yj = vs[j][1];
        var intersect = ((yi > y) != (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }
    return inside;
};

function doBbox(coords) {
    let lats = [];
    let lngs = [];

    for (var i = 0; i < coords[0].length; i++) {
        lats.push(coords[0][i][1]);
        lngs.push(coords[0][i][0]);
    }

    // calc the min and max lng and lat
    const minlat = Math.min.apply(null, lats),
        maxlat = Math.max.apply(null, lats),
        minlng = Math.min.apply(null, lngs),
        maxlng = Math.max.apply(null, lngs);

    // create a bounding rectangle that can be used in leaflet
    const bbox = [minlat, minlng, maxlat, maxlng];
    return bbox;
}

function getMoreNodes(elements,{outerPositions,innerPositions},nodesElements) {
    const moreNodes = [];
    if (elements.node) {
        elements.node.forEach(
            n => {
                const node = utils.flattenAttributes(n);
                const coord = [node.lat, node.lon];
                // const nodeInsideWay = pointInPolygon(coord, wayPositions);
                const nodeInsideWay = outerPositions.some(wayPositions=>pointInPolygon(coord, wayPositions));

                const nodePartOfWay = nodesElements.some(wn => wn.id == node.id)
                if (nodeInsideWay && !nodePartOfWay) {
                    const nodetags = n.tag ? n.tag.map(t => utils.flattenAttributes(t)) : null;
                    node.tag = nodetags;
                    moreNodes.push(node);
                }
            }
        );
    }
    return moreNodes;
}

module.exports = {
    pointInPolygon: pointInPolygon,
    doBbox: doBbox,
    getMoreNodes:getMoreNodes,
};