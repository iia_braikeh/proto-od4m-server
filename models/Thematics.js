const mongoose = require("mongoose");

ThematicConfigSchema = new mongoose.Schema({
    colors: {
        type: String,
        default: null
    },
    layers: {
        type: String,
        default: null
    },
    latlng: {
        type: {},
        default: null
    },

    zoom: {
        type: {},
        default: null
    }

});

// Create Schema
const ThematicSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        description: {
            type: String,
            required: true,
        },
        password: {
            type: String
        },
        referral_code: {
            type: String,
            default: function () {
                let hash = 0;
                for (let i = 0; i < this.email.length; i++) {
                    hash = this.email.charCodeAt(i) + ((hash << 5) - hash);
                }
                let res = (hash & 0x00ffffff).toString(16).toUpperCase();
                return "00000".substring(0, 6 - res.length) + res;
            }
        },
        referred_by: {
            type: String,
            default: null
        },
        creation_date: {
            type: Date,
            default: Date.now
        },
        last_date: {
            type: Date,
            default: Date.now
        },
        configuration: ThematicConfigSchema,
        owners: [User],
        changesets: {
            numbers: [Number]
        }

    },
    { strict: false }
);

module.exports = Thematic = mongoose.model("thematics", ThematicSchema);