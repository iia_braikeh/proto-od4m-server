# 1. Architecture choices


## Use cache for api routes
Date: 2020-09-14
### Aim
 - Avoid duplicate requests to osm server within a short time
 - 
 
## No ssl certs and https support for the express server
Date: 2020-10-6
### Aim
 - a reverse proxy is a more appropriate place to manage secure access to the server
 - 