require("dotenv").config();
// const bcrypt = require("bcryptjs");
const User = require("../models/Users");

const passport = require("passport"),
  // , util = require('util')
  OpenStreetMapStrategy = require("passport-openstreetmap").Strategy;


const SERVER_URL = process.env.NODE_ENV === "production" ? process.env.SERVER_PROD_URL : process.env.SERVER_URL;

const strategyOptions = OSM_API_END_POINT = process.env.OSM_API === 'dev' ?
  {
    consumerKey: process.env.OSM_DEV_CONSUMER_KEY,
    consumerSecret: process.env.OSM_DEV_CONSUMER_SECRET,
    callbackURL: `${SERVER_URL}/auth/openstreetmap/callback`,
    requestTokenURL:
      "https://master.apis.dev.openstreetmap.org/oauth/request_token",
    accessTokenURL:
      "https://master.apis.dev.openstreetmap.org/oauth/access_token",
    userAuthorizationURL:
      "https://master.apis.dev.openstreetmap.org/oauth/authorize",
  } : {
    consumerKey: process.env.OSM_CONSUMER_KEY,
    consumerSecret: process.env.OSM_CONSUMER_SECRET,
    callbackURL: `${SERVER_URL}/auth/openstreetmap/callback`,
    requestTokenURL:
      "https://www.openstreetmap.org/oauth/request_token",
    accessTokenURL:
      "https://www.openstreetmap.org/oauth/access_token",
    userAuthorizationURL:
      "https://www.openstreetmap.org/oauth/authorize",
    // successRedirect: '/auth/redirect',
    // successRedirect: `${process.env.CLIENT_URL}/profile`,
    // failureRedirect: '/auth/fail',
  };

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.  However, since this example does not
//   have a database of user records, the complete OpenStreetMap profile is
//   serialized and deserialized.

passport.serializeUser(function (user, done) {
  console.log("serialize : ", user);
  done(null, user.openstreetmapId);
});

passport.deserializeUser(function (openstreetmapId, done) {
  console.log("unSerialize : ", openstreetmapId);
  // User.findById(id, function (err, user) {
  User.findOne({ openstreetmapId: openstreetmapId }, function (err, user) {
    // console.log(user)
    done(err, user);
  });
});

// Use the OpenStreetMapStrategy within Passport.
//   Strategies in passport require a `verify` function, which accept
//   credentials (in this case, a token, tokenSecret, and OpenStreetMap profile), and
//   invoke a callback with a user object.

passport.use(
  new OpenStreetMapStrategy(
    {
      ...strategyOptions,
      provider: "openstreetmap",
      authScheme: "oauth",
      module: "passport-openstreetmap",
      scope: ["id", "displayName"],
      link: false
    },

    function (token, tokenSecret, profile, done) {
      console.log("...............");
      // with MongoDB
      User.findOrCreate(
        //   User.Create(
        {
          openstreetmapId: profile.id
        },
        {
          name: profile.displayName,
          changesetsCount: profile._xml2json.user.changesets["@"].count,
          token: token,
          tokenSecret: tokenSecret
        },

        function (err, user) {
          return done(err, user);
        }
      );

      // asynchronous verification, for effect...
      // process.nextTick(function () {
      //     //     // To keep the example simple, the user's OpenStreetMap profile is returned to
      //     //     // represent the logged-in user.  In a typical application, you would want
      //     //     // to associate the OpenStreetMap account with a user record in your database,
      //     //     // and return that user instead.
      //     return done(null, profile);
      // });
    }
  )
);
module.exports = passport;
